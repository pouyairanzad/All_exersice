package advance.android.rx_retrofit;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ShortcutInfo;
import android.content.pm.ShortcutManager;
import android.graphics.drawable.Icon;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.jakewharton.rxbinding.view.RxView;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import advance.android.GoogleMap.MapsActivity;
import advance.android.R;
import advance.android.customViews.MyTextView;
import advance.android.utils.Gen;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import rx.functions.Action1;

public class RxRetrofitActivity extends AppCompatActivity {
    MyTextView result;

    String name   ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rx_retrofit);
        result = findViewById(R.id.result);

        Toast.makeText(this, name, Toast.LENGTH_SHORT).show();

        showNotification();


        RxView.clicks(findViewById(R.id.show))
                .debounce(3, TimeUnit.SECONDS)
                .subscribe(V -> {
                    getData();
                });

        setShortcuts();
//        findViewById(R.id.show).setOnClickListener(V -> {
//            getData() ;
//        });

    }


    void showNotification() {

        Intent intent = new Intent(this, MapsActivity.class);
        PendingIntent pi = PendingIntent.getActivity(this, 0, intent, 0);

        NotificationManager mNotificationManager =
                (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("default" ,"app1Notification",
                    NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription("app1Notification");
            mNotificationManager.createNotificationChannel(channel);
        }

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, "default").
        setSmallIcon(R.mipmap.ic_launcher).
                setContentTitle("title").
                setContentText("content").
                setContentIntent(pi).
                setAutoCancel(true);
        mNotificationManager.notify(100, mBuilder.build());


    }

    void setShortcuts() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N_MR1) {
            ShortcutManager mgr = getSystemService(ShortcutManager.class);

            ShortcutInfo scanShortcut =
                    new ShortcutInfo.Builder(this, "advance.android").
                            setIcon(Icon.createWithResource(this, R.drawable.profile))
                            .setShortLabel("update profile")
                            .setLongLabel("you can update your profile")
                            .setIntent(new Intent(Intent.ACTION_VIEW, Uri.EMPTY, this, MapsActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK))
                            .build();

            ShortcutInfo scanShortcut2 =
                    new ShortcutInfo.Builder(this, "advance.android2").
                            setIcon(Icon.createWithResource(this, R.drawable.ic_launcher))
                            .setShortLabel("update profile2")
                            .setLongLabel("you can update your profile2")
                            .setIntent(new Intent(Intent.ACTION_VIEW, Uri.EMPTY, this, MapsActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK))
                            .build();

            if (mgr != null) {
                mgr.setDynamicShortcuts(Arrays.asList(scanShortcut, scanShortcut2));
            }


        }
    }


    private void getData() {
        RxRetroServiceGenerator.interFace.getUserDetails()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccess, this::onError, this::onComplete);
    }

    private void onComplete() {
        Gen.toast("onComplete");
    }

    private void onError(Throwable throwable) {
        Gen.toast("onError: " + throwable);
    }

    private void onSuccess(IpApiModel ipApiModel) {
        result.setText(ipApiModel.toString());
    }
}
