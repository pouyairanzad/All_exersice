package advance.android.rx_retrofit;

import io.reactivex.Observable;
import retrofit2.http.GET;

public interface IPAPIInterFace {


    @GET("json")
    Observable<IpApiModel> getUserDetails();


}
