package advance.android.GoogleMap;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;

import advance.android.R;
import advance.android.utils.Gen;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnCameraIdleListener {

    private GoogleMap mMap;
    String TAG = "location_" ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
//        LatLng sydney = new LatLng(-34, 151);
//        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));




        LatLng coordinate = new LatLng(35.724780, 51.421165);
        CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                coordinate, 15);
        mMap.animateCamera(location);


        mMap.setOnCameraIdleListener(this);


        mMap.getUiSettings().setZoomControlsEnabled(true);


        mMap.getUiSettings().setMyLocationButtonEnabled(true);

        MarkerOptions markerOptions = new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.marker));


        MarkerOptions locA = markerOptions.position(new LatLng(34.724780, 50.421165)).title("Marker in Sydney");
        MarkerOptions locB = markerOptions.position(new LatLng(34.824780, 50.221165)).title("Marker in Sydney");
        MarkerOptions locC = new MarkerOptions().position(new LatLng(34.924780, 50.321165)).title("Marker in Sydney");
        MarkerOptions locD = new MarkerOptions().position(new LatLng(34.124780, 50.921165)).title("Marker in Sydney");


        mMap.addMarker(locA) ;
        mMap.addMarker(locB) ;
        mMap.addMarker(locD) ;
        mMap.addMarker(locC) ;



        try{
            boolean mapChanged = mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.map_new_style));
            Log.d(TAG, "onMapReady: " + mapChanged);
        }catch (Exception e){
            Log.d(TAG, "Exception: " + e);
        }


    }

    @Override
    public void onCameraIdle() {
        double lat = mMap.getCameraPosition().target.latitude;
        double longi = mMap.getCameraPosition().target.longitude;

        Gen.toast("lat : " + lat + " long: " + longi);

    }
}
