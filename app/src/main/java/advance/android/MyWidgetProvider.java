package advance.android;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.RemoteViews;

public class MyWidgetProvider extends AppWidgetProvider {


    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        super.onUpdate(context, appWidgetManager, appWidgetIds);

        for(int id : appWidgetIds){
            RemoteViews remoteViews = new RemoteViews(
                    context.getPackageName(), R.layout.widget_layout
            );

            long unixTime = System.currentTimeMillis();
            remoteViews.setTextViewText(R.id.txt, unixTime + "");

            Intent actionIntent = new Intent(
                    context, MyWidgetProvider.class
            );
            actionIntent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
            actionIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, actionIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            remoteViews.setOnClickPendingIntent(R.id.btn,pendingIntent);

            appWidgetManager.updateAppWidget(id, remoteViews);
        }


    }
}
