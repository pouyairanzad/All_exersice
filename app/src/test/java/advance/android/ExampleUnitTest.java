package advance.android;

import org.junit.Test;

import advance.android.utils.Gen;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {

       // assertEquals(4, 2 + 2);

        assertEquals(23 , Gen.plus(14 , 6));
    }
}